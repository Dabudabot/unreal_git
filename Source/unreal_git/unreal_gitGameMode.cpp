// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_gitGameMode.h"
#include "unreal_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_gitGameMode::Aunreal_gitGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
